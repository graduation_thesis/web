<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="../../logo/dhcs.png">
    <title>TDKT CAND</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    @yield('css_head')
    <style>
        #cuon {
            display: block;
            position: fixed;
            bottom: 15px;
            right: 15px;
        }

        #phone-home {
            display: block;
            position: fixed;
            bottom: 15px;
            right: 25px;
            text-decoration: none;
        }
    </style>

</head>
<body>
@include('layouts.menu')
<div class="clr"></div>
@if(Auth::check())
@else
    @include('layouts.slider')
@endif
<div class="clr"></div>
<div class="">
    @yield('content')
</div>
{{--@include('layouts.common.chatbox')--}}
@include('layouts.common.croll')
@include('layouts.footer')

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

@yield('js_footer')

{{--CROLL TOP--}}
<script>
    window.$ = window.jQuery = $;

    $('' +
        '#cuon').click(function () {
        $('body,html').animate({scrollTop: 0}, 600);
        return false;
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() === 0) {
            $('#cuon').stop(false, true).fadeOut(300);
        } else {
            $('#cuon').stop(false, true).fadeIn(300);
        }
    });
</script>

</body>
</html>

<div class="bd-example">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://previews.123rf.com/images/maxborovkov/maxborovkov1811/maxborovkov181100363/111280895-blurred-shiny-happy-new-year-2020-banner-with-snowflakes-and-bokeh-backdrop-vector-background-.jpg"
                     class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="https://previews.123rf.com/images/maxborovkov/maxborovkov1811/maxborovkov181100363/111280895-blurred-shiny-happy-new-year-2020-banner-with-snowflakes-and-bokeh-backdrop-vector-background-.jpg"
                     class="d-block w-100" alt="...">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
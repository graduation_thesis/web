<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img src="../../logo/dhcs.png" class="img-responsive" height="35">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            @if(Auth::check())
            <ul class="navbar-nav ml-auto">
                <li class="nav-item {{ Route::currentRouteName() == 'document.get' ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('document.get')}}">Văn bản</a>
                </li>
                <li class="nav-item {{ Route::currentRouteName() == 'decision.get' ? 'active' : '' }}">
                    <a class="nav-link" href="{{route('decision.get')}}">Quyết định</a>
                </li>
            </ul>
                <a href="{{route('registerReward.get')}}" class="btn btn-warning text-info my-2 my-sm-0 ml-3">Đăng kí
                    thi đua cá nhân</a>
                <a href="{{route('registerCollectiveReward.get')}}" class="btn btn-danger text-white my-2 my-sm-0 ml-3">Đăng kí
                    thi đua đơn vị</a>
                <a href="{{route('profile.get')}}" class="btn btn-light text-info my-2 my-sm-0 ml-3">
                    {{Auth::user()->username}}
                </a>
                <a href="{{route('logout')}}" class="btn btn-outline-light my-2 my-sm-0 ml-3">Thoát</a>
            @else
                <ul class="navbar-nav ml-auto">
                </ul>
                <a href="{{route('login.get')}}" class="btn btn-outline-light my-2 my-sm-0 ml-3" style="float: right;">Đăng
                    Nhập</a>
            @endif
        </div>
    </div>
</nav>


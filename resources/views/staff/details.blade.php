@extends('layouts.common.menuPage')
@section('content')
    <div class="container">
        @foreach($data as $item)
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <a href="{{route('staff.download', ['id' => $item->code_number])}}" class="btn btn-dark"><b>Tải hồ
                            sơ</b></a>
                    <hr>
                    <li class="breadcrumb-item active" aria-current="page"><b>{{$item->full_name}}
                            - {{$item->units['name']}}</b></li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-md-12 mb-3">
                    <div class="row">
                        <div class="col">
                            <label>Họ tên cán bộ</label>
                            <input disabled type="text" value="{{$item->full_name}}"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="row">
                        <div class="col">
                            <label>Đơn vị trên cơ sở</label>
                            <input disabled type="text" value="{{$item->schools['name']}}"
                                   class="form-control">
                        </div>
                        <div class="col">
                            <label>Đơn vị cơ sở</label>
                            <input disabled type="text" value="{{$item->units['name']}}"
                                   class="form-control">
                        </div>
                        <div class="col">
                            <label>Đội/Đồn/Tổ</label>
                            @if(!$item->team_id)
                                <input disabled type="text" value="Chưa tham gia"
                                       class="form-control">
                            @else
                                <input disabled type="text" value="{{$item->teams['name']}}"
                                       class="form-control">
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="row">
                        <div class="col">
                            <label>Ngày sinh</label>
                            <input disabled type="date" value="{{$item->birthday}}"
                                   class="form-control">
                        </div>
                        <div class="col">
                            <label>Giới tính</label>
                            @if($item->sex === 0)
                                <input disabled type="text" value="Nữ"
                                       class="form-control">
                            @else
                                <input disabled type="text" value="Nam"
                                       class="form-control">
                            @endif
                        </div>
                        <div class="col">
                            <label>Số hiệu</label>
                            <input disabled type="text" value="{{$item->code_number}}"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="col-md-12 mb-3">
                    <div class="row">
                        <div class="col">
                            <label>Cấp bậc</label>
                            @if(!$item->rank_staff_id)
                                <input disabled type="text" value="Chưa có cấp bậc"
                                       class="form-control">
                            @else
                                <input disabled type="text" value="{{$item->ranks['name']}}"
                                       class="form-control">
                            @endif
                        </div>
                        <div class="col">
                            <label>Chức vụ</label>
                            @if(!$item->position_staff_id)
                                <input disabled type="text" value="Chưa có chức vụ"
                                       class="form-control">
                            @else
                                <input disabled type="text" value="{{$item->positions['name']}}"
                                       class="form-control">
                            @endif
                        </div>
                        <div class="col">
                            <label>Loại cán bộ</label>
                            @if(!$item->type_staff_id)
                                <input disabled type="text" value="Chưa có loại cán bộ"
                                       class="form-control">
                            @else
                                <input disabled type="text" value="{{$item->types['name']}}"
                                       class="form-control">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        <nav aria-label="breadcrumb" class="mt-5">
            <ol class="breadcrumb changeBacgroundCollor">
                <li class="breadcrumb-item active text-light" aria-current="page">
                    <b>Top 5 thành tích cá nhân</b>
                </li>
                <hr>
                <a href="{{route('personalReward.get', ['id' => $item->code_number])}}" class="btn btn-default"><b>Xem
                        thêm</b></a>
            </ol>
        </nav>

        <div>
            @include('staff.common.personal_reward')
        </div>
        {{--        ======================================    KHEN THƯỞNG TẬP THỂ--}}
        <nav aria-label="breadcrumb" class="mt-5">
            <ol class="breadcrumb changeBacgroundCollor">
                <li class="breadcrumb-item active text-light" aria-current="page">
                    <b>Top 5 thành tích tập thể</b>
                </li>
                <hr>
                <a href="{{route('collectiveReward.get', ['id' => $item->code_number])}}" class="btn btn-default"><b>Xem
                        thêm</b></a>
            </ol>
        </nav>

        {{--        ======================================    KỈ LUẬT--}}
        <div>
            @include('staff.common.collective_reward')
        </div>

        <nav aria-label="breadcrumb" class="mt-5">
            <ol class="breadcrumb changeBacgroundCollor">
                <li class="breadcrumb-item active text-light" aria-current="page">
                    <b>Top 5 kỷ luật</b>
                </li>
                <hr>
                <a href="{{route('discipline.get', ['id' => $item->code_number])}}" class="btn btn-default"><b>Xem
                        thêm</b></a>
            </ol>
        </nav>

        <div>
            @include('staff.common.discipline')
        </div>
    </div>
@endsection
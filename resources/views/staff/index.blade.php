@extends('layouts.common.menuPage')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><b>Danh sách cán bộ</b></li>
            </ol>
        </nav>
        <div>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Số hiệu</th>
                    <th>Họ tên</th>
                    <th>Giới tính</th>
                    <th>Cấp bậc</th>
                    <th>Chức vụ</th>
                    <th>Loại</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td>{{$loop->index + 1}}</td>
                        <td>{{$item->code_number}}</td>
                        <td>{{$item->full_name}}</td>
                        @if($item->sex === 0)
                        <td>Nam</td>
                        @else
                        <td>Nữ</td>
                        @endif
                        @if(!$item->rank_staff_id)
                            <td class="text-danger">Hiện chưa có cấp bậc</td>
                        @else
                            <td>{{$item->ranks['name']}}</td>
                        @endif
                        @if(!$item->position_staff_id)
                            <td class="text-danger">Hiện chưa có chức vụ</td>
                        @else
                            <td>{{$item->positions['name']}}</td>
                        @endif
                        @if(!$item->type_staff_id)
                            <td class="text-danger">Hiện chưa có loại cán bộ</td>
                        @else
                            <td>{{$item->types['name']}}</td>
                        @endif
{{--                        <td>{{date('Y-m-d', strtotime($item->created_at))}}</td>--}}
                        <td>
                            <a class="btn btn-info" href="{{route('staff.detail', ['id' => $item->code_number])}}"
                                    data-placement="right" title="Thông tin">
                                <i class="fas fa-eye"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
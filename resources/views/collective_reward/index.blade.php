@extends('layouts.common.menuPage')
@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb changeBacgroundCollor">
                <li class="breadcrumb-item active text-light" aria-current="page">
                    <b>Thành tích tập thể</b>
                </li>
                <hr>
            </ol>
        </nav>
        <div class="mb-3">
            @foreach($staff as $item)
                <h3>{{$item->full_name}} - {{$item->units['name']}}</h3>
            @endforeach
        </div>
        <div>
            @include('collective_reward.common.collective_reward')
        </div>
    </div>
@endsection
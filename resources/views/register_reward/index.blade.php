@extends('layouts.common.menuPage')

@section('css_head')
    <style>
        [type="file"] {
            height: 0;
            overflow: hidden;
            width: 0;
        }

        [type="file"] + label {
            background: #f15d22;
            border: none;
            border-radius: 5px;
            color: #fff;
            cursor: pointer;
            display: inline-block;
            font-family: 'Poppins', sans-serif;
            font-size: inherit;
            font-weight: 600;
            margin-bottom: 1rem;
            outline: none;
            padding: 1rem 50px;
            position: relative;
            transition: all 0.3s;
            vertical-align: middle;

        }

        span {
            display: inline-block;
            height: 100%;
            transition: all 0.3s;
            width: 100%;
        }

        .wrapper {
            background-color: #fff;
            border-radius: 1rem;
            margin: 0 auto;
            max-width: 1000px;
            padding: 2rem;
            width: 100%;
        }

        h1,
        p {
            margin-bottom: 2rem;
        }

        h1 {
            font-family: 'Poppins', sans-serif;
            font-size: 1.7rem;
        }

        a {
            color: #f15d22;
            text-decoration: none;
        }

        /*Custome table*/
        table {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }

        table th {
            background-color: #F7F7F7;
            color: #333;
            font-weight: bold;
        }

        table th, table td {
            padding: 5px;
            border: 1px solid #ccc;
        }

        .wrapper2 {
            background-color: #fff;
            border-radius: 1rem;
            margin: 0 auto;
            max-width: 1000px;
            padding: 2rem;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="wrapper">
        <h1>Đăng kí thi đua khen thưởng cho đơn vị của bạn! Tại sao không?</h1>
        <p>Vui lòng chọn tệp với định dạng .xlsx để có thể đăng kí danh sách?<a href=""> Bạn chưa có file?</a></p>
        @if(Session::has('flag'))
            <div class="alert alert-{{Session::get('flag')}} text-center" role="alert">
                <b>{{Session::get('get-notification')}}</b>
            </div>
        @endif
        <form action="{{route('registerReward.post')}}" method="POST" enctype="multipart/form-data">
            <div class="ml-5">
                <input type="file" name="file" id="fileUpload" accept=".xlsx" onchange="myFunction()"/>
                <label for="fileUpload"/>Chọn tệp Excel</label>
                <div style="float: right">
                    <b>Tệp bạn chọn: </b>
                    <p id="getUrlExcel">Hiện đang trống</p>
                </div>
            </div>
{{--            <input type="button" class="btn btn-info ml-5" id="upload" value="Xem trước" onclick="preview()"/>--}}
            <input type="submit" disabled class="btn btn-success ml-5" name="uploadPost" id="uploadPost"
                   value="Đăng kí"/>
            {{csrf_field()}}
        </form>
        <hr/>
        <div style="overflow-x:auto;" id="dvExcel">
        </div>
    </div>
    <section class="mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-business-time"></i>
                        </span>
                        <h5><span class="counter">Cập nhật thi đua liên tục</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated bounce">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-users"></i>
                        </span>

                        <h5><span class="counter">Đội ngũ chuyên nghiệp</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-money-check-alt"></i>
                        </span>

                        <h5><span class="counter">Công bằng văn minh</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="far fa-handshake"></i>
                        </span>

                        <h5><span class="counter">Chính xác và trách nhiệm</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js_footer')
    @parent
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
    <script type="text/javascript">
        function myFunction() {
            if(this.preview()){
                location.reload();
            }
            var x = document.getElementById("fileUpload");
            document.getElementById("getUrlExcel").innerHTML = x.files[0].name;
        }

        function preview() {
            //Reference the FileUpload element.
            var fileUpload = document.getElementById("fileUpload");
            //Validate whether File is valid Excel file.
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
            if (regex.test(fileUpload.value.toLowerCase())) {
                if (typeof (FileReader) != "undefined") {
                    var reader = new FileReader();

                    //For Browsers other than IE.
                    if (reader.readAsBinaryString) {
                        reader.onload = function (e) {
                            ProcessExcel(e.target.result);
                        };
                        reader.readAsBinaryString(fileUpload.files[0]);
                    } else {
                        //For IE Browser.
                        reader.onload = function (e) {
                            var data = "";
                            var bytes = new Uint8Array(e.target.result);
                            for (var i = 0; i < bytes.byteLength; i++) {
                                data += String.fromCharCode(bytes[i]);
                            }
                            ProcessExcel(data);
                        };
                        reader.readAsArrayBuffer(fileUpload.files[0]);
                    }
                } else {
                    alert("This browser does not support HTML5.");
                }
            } else {
                alert("Làm ơn upload tệp thuộc file excel.");
            }
        };

        function ProcessExcel(data) {
            //Read the Excel File data.
            var workbook = XLSX.read(data, {
                type: 'binary'
            });

            //Fetch the name of First Sheet.
            var firstSheet = workbook.SheetNames[0];

            //Read all rows from First Sheet into an JSON array.
            var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);

            //Create a HTML Table element.
            var table = document.createElement("table");
            table.border = "1";

            //Add the header row.
            var row = table.insertRow(-1);

            //Add the header cells.
            var headerCell = document.createElement("TH");
            headerCell.innerHTML = "id";
            row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "code_number";
            row.appendChild(headerCell);

            // headerCell = document.createElement("TH");
            // headerCell.innerHTML = "school_id";
            // row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "unit_id";
            row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "rank_staff_id";
            row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "personal_title_id";
            row.appendChild(headerCell);

            headerCell = document.createElement("TH");
            headerCell.innerHTML = "reward_date";
            row.appendChild(headerCell);

            //Add the data rows from Excel file.
            for (var i = 0; i < excelRows.length; i++) {
                //Add the data row.
                var row = table.insertRow(-1);

                //Add the data cells.
                var cell = row.insertCell(-1);
                // cell.innerHTML = excelRows[i].id;
                cell.innerHTML = i + 1;

                cell = row.insertCell(-1);
                cell.innerHTML = excelRows[i].code_number;

                if(excelRows[i].code_number === undefined)
                {
                    alert('Vui lòng kiểm tra lại. Đã có một hoặc nhiều cột bị undefind')
                    $("#uploadPost").attr("disabled", true);
                    break
                }

                // cell = row.insertCell(-1);
                // cell.innerHTML = excelRows[i].school_id;

                cell = row.insertCell(-1);
                cell.innerHTML = excelRows[i].unit_id;

                if(excelRows[i].unit_id === undefined)
                {
                    alert('Vui lòng kiểm tra lại. Đã có một hoặc nhiều cột bị undefind')
                    $("#uploadPost").attr("disabled", true);
                    break
                }

                cell = row.insertCell(-1);
                cell.innerHTML = excelRows[i].rank_staff_id;

                if(excelRows[i].rank_staff_id === undefined)
                {
                    alert('Vui lòng kiểm tra lại. Đã có một hoặc nhiều cột bị undefind')
                    $("#uploadPost").attr("disabled", true);
                    break
                }

                cell = row.insertCell(-1);
                cell.innerHTML = excelRows[i].personal_title_id;

                if(excelRows[i].personal_title_id === undefined)
                {
                    alert('Vui lòng kiểm tra lại. Đã có một hoặc nhiều cột bị undefind')
                    $("#uploadPost").attr("disabled", true);
                    break
                }

                cell = row.insertCell(-1);
                cell.innerHTML = excelRows[i].reward_date;

                if(excelRows[i].reward_date === undefined)
                {
                    alert('Vui lòng kiểm tra lại. Đã có một hoặc nhiều cột bị undefind')
                    $("#uploadPost").attr("disabled", true);
                    break
                }

                $("#uploadPost").attr("disabled", false);
            }
            var dvExcel = document.getElementById("dvExcel");
            dvExcel.innerHTML = "";
            dvExcel.appendChild(table);
        };
    </script>
@endsection

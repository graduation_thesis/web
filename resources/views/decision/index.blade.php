@extends('layouts.common.menuPage')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><b>Quyết định</b></li>
            </ol>
        </nav>
        <div>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Quyết định</th>
                    <th>Trích yếu nội dung</th>
                    <th>Tệp đính kèm</th>
                    {{--                    <th>Ban hành</th>--}}
                </tr>
                </thead>
                <tbody>
                @foreach($list as $item)
                    <tr>
                        <td width="60px">{{$loop->index + 1}}</td>
                        <td width="150px">{{$item->decision_id}}</td>
                        @if(!$item->description)
                            <td class="text-danger">Không</td>
                        @else
                            <td>{{$item->description}}</td>

                        @endif
                        @if(!$item->decision_file)
                            <td width="200px" class="text-danger">
                                Không
                            </td>
                        @else
                            <td width="200px">
                                <a target="_blank" href="http://127.0.0.1:8000/file/decision/{{$item->decision_file}}">
                                    {{$item->decision_file}}
                                </a>
                            </td>
                        @endif
{{--                        <td width="100">{{date('Y-m-d', strtotime($item->created_at))}}</td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
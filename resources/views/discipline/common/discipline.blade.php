<table id="example" class="table table-bordered">
    <thead>
    <tr>
        <th>STT</th>
        <th>Hình thức</th>
        <th>Số quyết định</th>
        <th>Cơ quan quyết định</th>
        <th>Thời gian</th>
    </tr>
    </thead>
    <tbody>
    @if(count($discipline) !== 0)
        @foreach($discipline as $item)
            <tr>
                <td>{{$loop->index + 1}}</td>
                <td>{{$item->forms_of_disciplines['name']}}</td>
                <td>{{$item->decision_id}}</td>
                <td>{{$item->decision_agencies['name']}}</td>
                <td>{{date('d-m-Y', strtotime($item->decisions['received_date']))}}</td>
            </tr>
        @endforeach
    @else
        <td colspan="6" class="text-center text-danger">Hiện chưa có thông tin</td>
    @endif
    </tbody>
</table>
{{--{!! $personal_reward->links() !!}--}}
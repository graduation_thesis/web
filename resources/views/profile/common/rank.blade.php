@extends('layouts.common.menuPage')
@section('content')
    <script src="http://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
    <script src="http://www.chartjs.org/samples/latest/utils.js"></script>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page"><b>Thống kê xếp hạng</b></li>
        </ol>
    </nav>
    <div id="container" style="width: 60%;">
        <canvas id="canvas"></canvas>
    </div>
</div>
<script>
    var chartdata = {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($Months); ?>,
            // labels: month,
            datasets: [
                {
                    label: 'Đơn vị',
                    backgroundColor: '#26B99A',
                    borderWidth: 1,
                    data: <?php echo json_encode($Data); ?>
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    }
    var ctx = document.getElementById('canvas').getContext('2d');
    new Chart(ctx, chartdata);
</script>

@endsection
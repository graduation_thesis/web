@extends('layouts.common.menuPage')
@section('content')
    <div class="container">
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                <p class="text-danger">{{ $error }}</p>
            </div>
        @endforeach
        <div class="row my-2">
            <div class="col-lg-12 order-lg-2">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="" data-target="#profile" data-toggle="tab" class="nav-link active">Thông tin căn
                            bản</a>
                    </li>
                    <li class="nav-item">
                        <a href="" data-target="#edit" data-toggle="tab" class="nav-link">Tài khoản</a>
                    </li>
                </ul>
                @foreach($profile as $item)
                    <div class="tab-content py-4">
                        <div class="tab-pane active" id="profile">
                            <h5 class="mb-3">{{$item->full_name}} - ({{$item->birthday}})</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <h6>Cấp bậc</h6>
                                    <p>
                                        {{$item->ranks->name}}
                                    </p>
                                    <h6>Chức vụ</h6>
                                    <p>
                                        {{$item->positions->name}}
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <h6>Thành tích</h6>
                                    <span class="badge badge-primary"><i class="fas fa-star"></i> {{$count_personal_reward}} danh hiệu cá nhân</span>
                                    <span class="badge badge-success"><i class="fas fa-boxes"></i> {{$count_collective_reward}} danh hiệu tập thể</span>
                                    <span class="badge badge-danger"><i class="fas fa-exclamation-triangle"></i> {{$count_discipline}} kỷ luật</span>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Khác</h5>
                                    <table class="table table-sm table-hover table-striped">
                                        <tbody>
                                        <tr>
                                            <td>Đơn vị trên cơ sở:</td>
                                            <td class="text-right">
                                                <strong>{{$item->schools->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Đơn vị cơ sở:</td>
                                            <td class="text-right">
                                                <strong>{{$item->units->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Đội/Đồn/Tổ:</td>
                                            <td class="text-right">
                                                @if($item->team_id === null)
                                                    <strong>Chưa tham gia</strong>
                                                @else
                                                    <strong>{{$item->teams->name}}</strong>
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Cán bộ thuộc loại:</td>
                                            <td class="text-right">
                                                <strong>{{$item->types->name}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Số hiệu:</td>
                                            <td class="text-right">
                                                <strong>{{$item->code_number}}</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Giới tính:</td>
                                            <td class="text-right">
                                                @if($item->sex === 0)
                                                    <strong>Nam</strong>
                                                @else
                                                    <strong>Nữ</strong>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--/row-->
                        </div>
                        <div class="tab-pane" id="edit">
                            <form role="form" method="POST" action="{{ route('change.password') }}">
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Tài khoản</label>
                                    <div class="col-lg-9">
                                        <input readonly class="form-control" type="text" value="{{$account->username}}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Mật khẩu hiện tại</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" name="current_password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Mật khẩu mới</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" name="new_password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label">Xác nhận mật khẩu
                                        mới</label>
                                    <div class="col-lg-9">
                                        <input class="form-control" type="password" name="new_confirm_password">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-lg-3 col-form-label form-control-label"></label>
                                    <div class="col-lg-9">
                                        <input readonly type="reset" class="btn btn-secondary" value="Hủy">
                                        <input readonly type="submit" class="btn btn-primary" value="Cập nhật">
                                    </div>
                                </div>
                                {{csrf_field()}}
                            </form>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
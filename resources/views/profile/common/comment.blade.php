@extends('layouts.common.menuPage')

@section('css_head')
    @parent
    <style>
        #overflowTest {
            max-height: 450px;
            /*overflow: scroll;*/
            overflow: auto;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><b>Ý kiến - Phản hồi</b></li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-md-8">
                <div id="overflowTest">
                    @foreach($list as $item)
                        <div class="media">
                            @if($item->status === 1 && $item->feedback === 'Author')
                                <div class="media-body ml-3">
                                    <h5 class="media-heading text-danger">
                                        Quản trị viên
                                        <small>{{date('d-m-Y', strtotime($item->created_at))}}</small>
                                    </h5>
                                    {{$item->content}}
                                </div>
                            @else
                                <div class="media-body ml-3">
                                    <h5 class="media-heading">
                                        Tôi
                                        <small>{{date('d-m-Y', strtotime($item->created_at))}}</small>
                                    </h5>
                                    {{$item->content}}
                                </div>
                            @endif
                        </div>
                        <hr>
                    @endforeach
                </div>
                <hr>
                <div class="form-group">
                    <form method="POST">
                        <div class="m-1">
                            <label for="comment"><strong>Viết ý kiến phải hồi...</strong></label>
                            <i class="fas fa-pencil-alt"></i>
                            <label style="float:right">
                                <button type="submit" class="btn btn-success">Gửi</button>
                            </label>
                        </div>
                        <textarea class="form-control" rows="5" name="content_comment" id="content_comment"></textarea>
                        {{csrf_field()}}
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page"><b>Một số lưu ý khi phản hồi!</b></li>
                    </ol>
                </nav>
                <ol>
                    <li>Dùng từ ngữ văn minh, lịch sự.</li>
                    <li>Chú ý mục đích, nội dung của câu hỏi.</li>
                    <li>Không được chèn hình. Xin để đường dẫn vào bên trong để làm đường dẫn minh họa.</li>
                    <li class="text-danger">Sau khi đăng ý kiến phải hồi. Vui lòng chờ đợi để phía quản trị tiếp nhận và giải quyết!</li>
                </ol>
            </div>
        </div>
    </div>
@endsection
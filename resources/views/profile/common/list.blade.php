@extends('layouts.common.menuPage')
@section('content')

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><b>Đăng kí thi đua</b></li>
            </ol>
        </nav>
        <div>
            <table id="example" class="table table-bordered" style="width:100%">
                <thead>
                <tr>
                    <th>STT</th>
                    <th>Số hiệu cán bộ</th>
                    <th>Họ tên</th>
                    <th>Danh hiệu đăng kí</th>
                    <th>Thời gian</th>
                </tr>
                </thead>
                <tbody>
                @foreach($listRegisterReward as $item)
                <tr>
                    <td>{{$loop->index + 1}}</td>
                    <td>{{$item->code_number}}</td>
                    <td>{{$item->staffs['full_name']}}</td>
                    <td>{{$item->personal_titles['name']}}</td>
                    <td>{{date('d-m-Y', strtotime($item->reward_date))}}</td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
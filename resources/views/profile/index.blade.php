@extends('layouts.common.menuPage')
@section('content')
    <div class="container mt-5">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page"><b>Thông tin tổng quan</b></li>
            </ol>
        </nav>
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                <div class="card">
                    <a href="{{route('profileInfo.get')}}">
                        <img class="card-img-top"
                             src="https://elearning.tdmu.edu.vn/pix/tdmu/khoa-supham@3x.png"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">Thông tin cá nhân</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                <a href="{{route('staff.get')}}">
                    <div class="card">
                        <img class="card-img-top"
                             src="https://elearning.tdmu.edu.vn/pix/tdmu/khoa-kkte@3x.png"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">Danh sách cán bộ</p>
                        </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                <a href="{{route('profileRegisterReward.get')}}">
                    <div class="card">
                        <img class="card-img-top"
                             src="https://elearning.tdmu.edu.vn/pix/tdmu/khoa-cntk@3x.png"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">Danh sách đăng kí</p>
                        </div>
                    </div>
                </a>
            </div>

            {{--            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">--}}
            {{--                <div class="card">--}}
            {{--                    <a href="{{route('rank.get')}}">--}}
            {{--                        <img class="card-img-top"--}}
            {{--                             src="https://elearning.tdmu.edu.vn/pix/tdmu/khoa-kkte@3x.png"--}}
            {{--                             alt="Card image cap">--}}
            {{--                        <div class="card-body">--}}
            {{--                            <p class="card-text">Bảng xếp hạng</p>--}}
            {{--                        </div>--}}
            {{--                    </a>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                <div class="card">
                    <a href="{{route('comment.get')}}">
                        <img class="card-img-top"
                             src="https://elearning.tdmu.edu.vn/pix/tdmu/khoa-khtn@3x.png"
                             alt="Card image cap">
                        <div class="card-body">
                            <p class="card-text">
                                Ý kiến - phản hồi
                            </p>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
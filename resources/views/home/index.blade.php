@extends('layouts.basic')

@section('content')
    @if(Auth::check())
        <section class="mt-5 mb-5">
            <div class="container">
                <div class="row">
                    @include('profile.index')
                </div>
            </div>
        </section>
    @else
        <section class="mt-5 mb-5">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-business-time"></i>
                        </span>
                            <h5><span class="counter">Cập nhật thi đua liên tục</span></h5>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="text-center mb-2 animated bounce">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-users"></i>
                        </span>

                            <h5><span class="counter">Đội ngũ chuyên nghiệp</span></h5>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-money-check-alt"></i>
                        </span>

                            <h5><span class="counter">Công bằng văn minh</span></h5>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                        <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="far fa-handshake"></i>
                        </span>

                            <h5><span class="counter">Chính xác và trách nhiệm</span></h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    @endif
@endsection
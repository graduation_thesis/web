<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

// LOGIN
Route::get('dang-nhap', 'LoginController@getLogin')->name('login.get');
Route::post('dang-nhap', 'LoginController@postLogin')->name('login.post');

// LOGOUT
Route::get('dang-xuat', 'LoginController@logout')->name('logout');

// RESTART PASSWORD
Route::get('khoi-phuc-mat-khau', 'ResetPasswordController@getForm')->name('get.password.reset');
Route::post('khoi-phuc-mat-khau', 'ResetPasswordController@sendCode');

Route::get('mat-khau/cap-nhat', 'ResetPasswordController@resetPassword')->name('update.password');
Route::post('mat-khau/cap-nhat', 'ResetPasswordController@updatePassword')->name('update.password');

// IMPORT REGISTER COLLECTIVE REWARD
Route::get('dang-ki-thi-dua', 'RegisterRewardController@getForm')->name('registerReward.get');
Route::post('dang-ki-thi-dua', 'RegisterRewardController@import')->name('registerReward.post');

// IMPORT REGISTER REWARD
Route::get('dang-ki-thi-dua-tap-the', 'RegisterCollectiveRewardController@getForm')->name('registerCollectiveReward.get');
Route::post('dang-ki-thi-dua-tap-the', 'RegisterCollectiveRewardController@postForm')->name('registerCollectiveReward.post');

// PROFILE
Route::get('thong-tin', 'ProfileController@index')->name('profile.get');
Route::group(['prefix' => 'thong-tin'], function () {
    Route::get('dang-ki-thi-dua', 'ProfileController@getRegisterReward')->name('profileRegisterReward.get');
    Route::get('ca-nhan', 'ProfileController@getInfo')->name('profileInfo.get');
});
// Change Password
Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');


// CÁN BỘ
Route::group(['prefix' => 'can-bo'], function () {
    Route::get('danh-sach', 'StaffController@index')->name('staff.get');
    Route::get('chi-tiet/{id}', 'StaffController@details')->name('staff.detail');
});

// CÁN BỘ CÓ THÀNH TÍCH KHEN THƯỞNG CÁ NHÂN
Route::group(['prefix' => 'thanh-tich'], function () {
    Route::get('ca-nhan/{id}', 'PersonalRewardController@index')->name('personalReward.get');
    Route::get('tap-the/{id}', 'CollectiveRewardController@index')->name('collectiveReward.get');
});

// CÁN BỘ CÓ KỶ LUẬT
Route::group(['prefix' => 'ky-luat'], function () {
    Route::get('ca-nhan/{id}', 'DisciplineController@index')->name('discipline.get');
});


// DECISION
Route::group(['prefix' => 'quyet-dinh'], function () {
    Route::get('chi-tiet', 'DecisionController@index')->name('decision.get');
});

// DOCUMENT
Route::group(['prefix' => 'van-ban'], function () {
    Route::get('chi-tiet', 'DocumentController@index')->name('document.get');
});

// RANK
Route::get('xep-hang', 'RankController@index')->name('rank.get');

// IDEAL - RESPONSE - COMMENT
Route::get('y-kien-phan-hoi', 'CommentController@index')->name('comment.get');
Route::post('y-kien-phan-hoi', 'CommentController@postComment')->name('comment.post');


// TEST DOWNLOAD EXCEL
Route::get('excel/{id}', 'StaffController@download')->name('staff.download');


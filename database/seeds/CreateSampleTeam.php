<?php

use App\team;
use Illuminate\Database\Seeder;

class CreateSampleTeam extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/team.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing team ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $team = team::create($item);
            $team->save();
        }

        echo PHP_EOL;
    }
}

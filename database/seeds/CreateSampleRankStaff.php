<?php

use App\rank_staff;
use Illuminate\Database\Seeder;

class CreateSampleRankStaff extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/rank_staff.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing rank staff ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $rank_staff = rank_staff::create($item);
            $rank_staff->save();
        }

        echo PHP_EOL;
    }
}

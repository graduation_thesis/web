<?php

use App\forms_of_discipline;
use Illuminate\Database\Seeder;

class CreateSampleFormsOfDiscipline extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/forms_of_discipline.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing forms of discipline ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $forms_of_discipline = forms_of_discipline::create($item);
            $forms_of_discipline->save();
        }

        echo PHP_EOL;
    }
}

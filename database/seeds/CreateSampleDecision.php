<?php

use Illuminate\Database\Seeder;
use App\decision;

class CreateSampleDecision extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/decision.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing decision ${idx}/${c}";

            if (empty($item['decision_id'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $decision = decision::create($item);
            $decision->save();
        }

        echo PHP_EOL;
    }
}

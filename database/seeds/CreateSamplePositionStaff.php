<?php

use App\position_staff;
use Illuminate\Database\Seeder;

class CreateSamplePositionStaff extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $content = file_get_contents(__DIR__ . '/data/position_staff.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing position staff ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $position_staff = position_staff::create($item);
            $position_staff->save();
        }

        echo PHP_EOL;
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CreateSampleSchool::class);
        $this->call(CreateSampleUnit::class);
        $this->call(CreateSampleTypeStaff::class);
        $this->call(CreateSampleRankStaff::class);
        $this->call(CreateSamplePositionStaff::class);
        $this->call(CreateSamplePersonalTitle::class);
        $this->call(CreateSampleDecision::class);
        $this->call(CreateSampleDecisionAgency::class);
        $this->call(CreateSampleFormsOfReward::class);
        $this->call(CreateSampleFormsOfDiscipline::class);
        $this->call(CreateSampleTeam::class);
        $this->call(CreateSampleStaff::class);
        $this->call(CreateSampleFormsRisk::class);
        $this->call(CreateSampleUser::class);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegisterRewardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('register_rewards', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->comment('Mã user, khóa ngoại');
            $table->string('code_number')->comment('Số hiệu cán bộ, khóa ngoại');
            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->string('rank_staff_id')->comment('Mã cấp bậc, khóa ngoại');
            $table->string('personal_title_id')->comment('Mã danh hiệu cá nhân, khóa ngoại');
            $table->date('reward_date')->nullable()->comment('Ngày thi đua');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index(['user_id']);
            $table->index(['code_number']);
            $table->index(['personal_title_id']);

            $table->foreign('code_number')
                ->references('code_number')->on('staff')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('school_id')
                ->references('school_id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('rank_staff_id')
                ->references('rank_staff_id')->on('rank_staffs')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('personal_title_id')
                ->references('personal_title_id')->on('personal_titles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('register_rewards');
    }
}

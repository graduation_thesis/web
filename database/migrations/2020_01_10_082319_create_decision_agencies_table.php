<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDecisionAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('decision_agencies', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('decision_agency_id')->unique()->comment('Mã cơ quan quyết định');

            $table->string('name')->comment('Tên cơ quan quyết định');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('decision_agencies');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('code_number')->comment('Số hiệu cán bộ, khóa ngoại');
            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');

            $table->string('username')->unique()->comment('Tên tài khoản');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');

            // Quyền hạn
            $table->string('role')->default('author');
            $table->boolean('active')->default(true)->comment('Tình trạng khóa tài khoản');

            $table->rememberToken();

            $table->index('code_number');
            $table->index('school_id');
            $table->index('unit_id');

            $table->foreign('school_id')
                ->references('school_id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('code_number')
                ->references('code_number')->on('staff')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

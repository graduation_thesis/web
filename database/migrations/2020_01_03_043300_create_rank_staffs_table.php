<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRankStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rank_staffs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('rank_staff_id')->comment('Mã cấp bậc');

            $table->string('name')->default('')->comment('Tên cấp bậc');

            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index('rank_staff_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rank_staffs');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectiveRewardResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collective_reward_results', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id')->comment('Mã user, khóa ngoại');
            $table->string('school_id')->comment('Mã đơn vị trên cơ sở, khóa ngoại');
            $table->string('unit_id')->comment('Mã đơn vị cơ sở, khóa ngoại');
            $table->string('collective_title_id')->comment('Mã danh hiệu tập thể, khóa ngoại');

            // Thông tin người duyệt
            $table->unsignedBigInteger('approved_by')->nullable()->comment('Người phê duyệt');
            $table->date('reward_date')->nullable()->comment('Ngày thi đua');
            $table->timestamp('pending_remove')->nullable()->comment('Thời gian xóa vĩnh viễn');

            $table->index(['user_id']);
            $table->index(['collective_title_id']);

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('school_id')
                ->references('school_id')->on('schools')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('unit_id')
                ->references('unit_id')->on('units')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->foreign('collective_title_id')
                ->references('collective_title_id')->on('collective_titles')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collective_reward_results');
    }
}

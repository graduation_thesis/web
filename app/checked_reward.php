<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class checked_reward extends Model
{
    protected $fillable = [
        'user_id',
        'code_number',
        'school_id',
        'unit_id',
        'rank_staff_id',
        'personal_title_id',
        'reward_date',
        'pending_remove',
    ];

    public function users()
    {
        return $this->belongsTo(User::class, 'user_id','id');
    }

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'code_number','code_number');
    }

    public function personal_titles()
    {
        return $this->belongsTo(personal_title::class, 'personal_title_id','personal_title_id');
    }
}

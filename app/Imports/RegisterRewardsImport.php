<?php

namespace App\Imports;

use App\register_reward;
use Auth;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class RegisterRewardsImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new register_reward([
            //
//            'user_id'                       => $row['user_id'],
            'user_id'                       => Auth::user()->id,
            'code_number'                   => $row['code_number'],
            'school_id'                     => 'DHCSND',
            'unit_id'                       => $row['unit_id'],
            'rank_staff_id'                 => $row['rank_staff_id'],
            'personal_title_id'             => $row['personal_title_id'],
            'reward_date'                   => Date::excelToDateTimeObject($row['reward_date']),
            'pending_remove'                => null,
        ]);
    }
}

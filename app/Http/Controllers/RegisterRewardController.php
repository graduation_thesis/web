<?php

namespace App\Http\Controllers;
use App\Imports\RegisterRewardsImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class RegisterRewardController extends Controller
{
    public function getForm()
    {
        if(Auth::check()) {
            return view('register_reward.index');
        }
        return view('error.index');
    }

    public function import()
    {
        if(Auth::check()) {
            if (!request()->file('file')) {
                return redirect()->route('registerReward.get')->with(['flag' => 'danger', 'get-notification' => 'Vui lòng chọn tệp trước khi đăng kí!']);
            }
            Excel::import(new RegisterRewardsImport, request()->file('file'));

            return redirect()->route('registerReward.get')->with(['flag' => 'success', 'get-notification' => 'Đăng kí danh hiệu cá nhân thành công!']);
        }
        return view('error.index');
    }
}

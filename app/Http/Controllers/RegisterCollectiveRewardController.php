<?php

namespace App\Http\Controllers;

use App\collective_title;
use App\register_collective_reward;
use Illuminate\Http\Request;
use Auth;

class RegisterCollectiveRewardController extends Controller
{
    public function getForm()
    {
        if(Auth::check()) {
            $listCollectiveTitle = collective_title::all();

            return view('register_collective_reward.index', [
                'listCollectiveTitle' => $listCollectiveTitle
            ]);
        }
        return view('error.index');
    }

    public function postForm(Request $request)
    {
        if(Auth::check()) {
            $getTitle = $request->get('getTitle');
            $getRewardDate = $request->get('reward_date');

            if (!$getTitle) {
                return redirect()->route('registerCollectiveReward.get')->with(['flag' => 'danger', 'get-notification' => 'Làm ơn chọn danh hiệu muốn đăng kí!']);
            }
            $regiter_collective_reward = new register_collective_reward();

            $regiter_collective_reward->user_id = auth()->user()->id;
            $regiter_collective_reward->school_id = auth()->user()->school_id;
            $regiter_collective_reward->unit_id = auth()->user()->unit_id;
            $regiter_collective_reward->collective_title_id = $getTitle;
            $regiter_collective_reward->reward_date = $getRewardDate;

            $regiter_collective_reward->save();

            return redirect()->route('registerCollectiveReward.get')->with(['flag' => 'success', 'get-notification' => 'Đăng kí danh hiệu cho đơn vị thành công!']);
        }
        return view('error.index');
    }
}

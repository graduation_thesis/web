<?php

namespace App\Http\Controllers;
use App\decision;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class DecisionController extends Controller
{
    public function index()
    {
        if(Auth::check())
        {
            $carbon = new Carbon();
            $dt = Carbon::now();
            $list = decision::orderBy('created_at', 'DESC')->get();

            return view('decision.index', [
                'list' => $list,
                'carbon' => $carbon,
                'dt' => $dt
            ]);
        }
        return view('error.index');
    }
}

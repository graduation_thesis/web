<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login.index');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required',
                'password' => 'required|min:6|max:20',
            ],
            [
                'username.required'         => 'Vui lòng nhập tài khoản!',
                'password.required'         => 'Vui lòng nhập mật khẩu',
                'password.min'              => 'Mật khẩu ít nhất 6 kí tự',
                'password.max'              => 'Mật khẩu không quá 20 kí tự',
            ]
        );

        $credentials = array('username' => $request->username, 'password'=>$request->password, 'role' => 'author');
        if(Auth::attempt($credentials))
        {
            return redirect()->route('home')->with(['flag'=>'success','login-notification'=>'Đăng nhập thành công!']);
        }
        else{
            return redirect()->back()->with(['flag'=>'danger','login-notification'=>'Đăng nhập thất bại! Vui lòng kiểm tra lại tài khoản hoặc mật khẩu?']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }
}

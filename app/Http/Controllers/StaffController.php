<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\discipline;
use App\personal_reward;
use App\staff;
use Carbon\Carbon;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Auth;
use App\Exports\InvoicesExport;

class StaffController extends Controller
{
    public function index()
    {
        if(Auth::check()) {
            $carbon = new Carbon();
            $dt = Carbon::now();
            $list = staff::with('schools', 'units', 'teams', 'ranks', 'positions', 'types')
                ->where('unit_id', '=', Auth::user()->unit_id)
                ->get();

            return view('staff.index', [
                'list' => $list,
                'carbon' => $carbon,
                'dt' => $dt
            ]);
        }
        return view('error.index');
    }

    public function details($id)
    {
        if(Auth::check()) {
            $data = staff::where('code_number', '=', $id)->get();

            $personal_reward = personal_reward::where('code_number', $id)->take(5)->get();
            $collective_reward = collective_reward::where('unit_id', auth::user()->unit_id)->take(5)->get();

            $discipline = discipline::where('code_number', $id)->take(5)->get();

            return view('staff.details', [
                'data' => $data,
                'personal_reward' => $personal_reward,
                'collective_reward' => $collective_reward,
                'discipline' => $discipline
            ]);
        }
        return view('error.index');
    }

    public function download($id)
    {
        return (new InvoicesExport($id))->download('Ho_So_Ca_Nhan.xlsx');
    }
}

<?php

namespace App\Http\Controllers;

use App\discipline;
use App\staff;
use Illuminate\Http\Request;
use Auth;

class DisciplineController extends Controller
{
    public function index($id)
    {
        if(Auth::check()) {
            $staff = staff::query()->where('code_number', $id)->get();
            $discipline = discipline::query()->where('code_number', $id)->get();

            return view('discipline.index', [
                'staff' => $staff,
                'discipline' => $discipline
            ]);
        }
        return view('error.index');
    }
}

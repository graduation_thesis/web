<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\comment;
use Auth;
use Carbon\Carbon;

class CommentController extends Controller
{
    public function index()
    {
        if(Auth::check()) {
            $list = comment::where('user_id', '=', Auth::user()->id)->get();
            return view('profile.common.comment', [
                'list' => $list
            ]);
        }
        return view('error.index');
    }

    public function postComment(Request $request)
    {
        if(Auth::check()) {
            $comment = new comment();
            $comment->user_id = Auth::user()->id;
            $comment->content = $request->content_comment;
            $comment->feedback = 'Administrator';
            $comment->status = false;
            $comment->created_at = Carbon::now();
            $comment->save();

            return back();
        }
        return view('error.index');
    }
}

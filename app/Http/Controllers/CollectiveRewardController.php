<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\staff;
use Illuminate\Http\Request;

class CollectiveRewardController extends Controller
{
    public function index($id)
    {
        $staff = staff::query()->where('code_number', $id)->get();
        $collective_reward = collective_reward::query()->where('unit_id', auth()->user()->unit_id)->get();

        return view('collective_reward.index', [
            'staff' =>$staff,
            'collective_reward' =>$collective_reward
        ]);
    }
}

<?php

namespace App\Http\Controllers;
use App\document;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class DocumentController extends Controller
{
    public function index()
    {
        if(Auth::check()) {
            $carbon = new Carbon();
            $dt = Carbon::now();
            $list = document::orderBy('created_at', 'DESC')->get();

            return view('document.index', [
                'list' => $list,
                'carbon' => $carbon,
                'dt' => $dt
            ]);
        }
        return view('error.index');
    }
}

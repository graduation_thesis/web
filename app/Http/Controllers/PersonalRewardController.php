<?php

namespace App\Http\Controllers;

use App\personal_reward;
use App\staff;
use Illuminate\Http\Request;
use Auth;

class PersonalRewardController extends Controller
{
    public function index($id)
    {
        if(Auth::check()) {
            $staff = staff::query()->where('code_number', $id)->get();
            $personal_reward = personal_reward::query()->where('code_number', $id)->get();

            return view('personal_reward.index', [
                'staff' => $staff,
                'personal_reward' => $personal_reward
            ]);
        }
        return view('error.index');
    }
}

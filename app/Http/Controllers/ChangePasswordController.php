<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use App\User;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('changePassword');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $request->validate([
            'current_password'                      => ['required', new MatchOldPassword],
            'new_password'                          => ['required'],
            'new_confirm_password'                  => ['same:new_password'],
        ], [
                'current_password.required'         => 'Mật khẩu cũ không được bỏ trống!',
                'new_password.required'             => 'Vui lòng nhập mật khẩu mới',
                'new_confirm_password.same'         => 'Mật khẩu xác nhận không trùng khớp!',
            ]
        );

        User::find(auth()->user()->id)->update(['password' => Hash::make($request->new_password)]);

        return redirect()->route('login.get')->with(['flag' => 'success', 'login-notification' => 'Đổi mật khẩu thành công!']);
    }
}

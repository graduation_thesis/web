<?php

namespace App\Http\Controllers;

use App\collective_reward;
use App\unit;
use Illuminate\Http\Request;
use Auth;

class RankController extends Controller
{
    public function index2()
    {
        if(Auth::check()) {
            $listCollectiveReward = collective_reward::all();
            $countListCollectiveReward = count($listCollectiveReward);

            $listUnit = unit::all();

            return view('profile.common.rank', [
                'listCollectiveReward' => $listCollectiveReward,
                'countListCollectiveReward' => $countListCollectiveReward,
                'listUnit' => $listUnit
            ]);
        }
        return view('error.index');
    }

    public function index()
    {
        if(Auth::check()) {
            $listCollectiveReward = collective_reward::with('units',
                'forms_of_rewards',
                'collective_titles',
                'decision_agencies',
                'decisions')
                ->get();

            foreach ($listCollectiveReward as $item) {
                $listUnit[] = $item['unit_id'];
            }
            $data = array(1, 2, 3, 4, 5, 6);
            return view('profile.common.rank', ['Months' => $listUnit, 'Data' => $data]);
        }
        return view('error.index');
    }
}

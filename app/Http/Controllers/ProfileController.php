<?php

namespace App\Http\Controllers;

use App\checked_reward;
use App\collective_reward;
use App\personal_reward;
use App\register_reward;
use App\staff;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        if(Auth::check()) {
            return view('profile.index');
        }
        return view('error.index');
    }

    public function getRegisterReward()
    {
        if(Auth::check()) {
            $carbon = new Carbon();
            $dt = Carbon::now();
            $listRegisterReward = checked_reward::with('users',
                'staffs',
                'personal_titles')
                ->where('unit_id', '=', Auth::user()->unit_id)
                ->get();

            return view('profile.common.list', [
                'listRegisterReward' => $listRegisterReward,
                'carbon' => $carbon,
                'dt' => $dt
            ]);
        }
        return view('error.index');
    }

    public function getInfo()
    {
        if(Auth::check()) {
            $profile = staff::with('schools', 'units', 'teams', 'ranks', 'positions', 'types')
                ->where('code_number', '=', Auth::user()->code_number)
                ->get();

            $account = Auth::user();

            $count_personal_reward = personal_reward::where('code_number', '=', auth()->user()->code_number)->count();
            $count_collective_reward = collective_reward::where('unit_id', '=', auth()->user()->unit_id)->count();
            $count_discipline = personal_reward::where('code_number', '=', auth()->user()->code_number)->count();

            return view('profile.common.info', [
                'profile' => $profile,
                'account' => $account,
                'count_personal_reward' => $count_personal_reward,
                'count_collective_reward' => $count_collective_reward,
                'count_discipline' => $count_discipline
            ]);
        }
        return view('error.index');
    }
}
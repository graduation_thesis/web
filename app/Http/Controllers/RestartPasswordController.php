<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;

class RestartPasswordController extends Controller
{
    public function getForm()
    {
        return view('login.reset.sendemail');
    }

    public function sendCode(Request $request)
    {
        $email = $request->email;
        $checkUser = User::where('email', $email)->first();
        if (!$checkUser) {
            return redirect()->back()->with('danger', 'Email không tồn tại!');
        }

        $code = bcrypt(md5(time() . $email));
        $checkUser->code = $code;
        $checkUser->time_code = Carbon::now();
        $checkUser->save();

        // Gửi mail nhận để reset
        $url = route('update.password', ['code' => $checkUser->code, 'email' => $email]);
        $data = [
            'route' => $url
        ];
        Mail::send('login.reset.email', $data, function ($message) use ($email) {
            $message->to($email, 'Reset Password')->subject('Lấy lại mật khẩu');
        });

        return redirect()->back()->with('success', 'Link lấy lại mật khẩu đã được gửi vào email của bạn!');
    }

    public function resetPassword(Request $request)
    {
        $code = $request->code;
        $email = $request->email;

        $checkUser = User::where(['code' => $code, 'email' => $email])->first();

        if (!$checkUser) {
            dd($checkUser);
            return redirect()->back()->with('danger', 'Xin lỗi! Đường dẫn lấy lại mật khẩu không đúng, bạn vui lòng thử lại sau');
        }

        return view('login.reset.resetpass');
    }

    public function updatePassword(RequestResetPassword $requestResetPassword)
    {

        $code = $requestResetPassword->code;
        $email = $requestResetPassword->email;

        $checkUser = User::where(['code' => $code, 'email' => $email])->first();

        if (!$checkUser) {
            return redirect()->back()->with('danger', 'Xin lỗi! Đường dẫn lấy lại mật khẩu không đúng, bạn vui lòng thử lại sau');
        }

        $checkUser->password = Hash::make($requestResetPassword->password_reset);
        $checkUser->save();

        return redirect()->back()->with('success', 'Mật khẩu đã được thay đổi thành công!');

    }
}

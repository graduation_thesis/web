<?php

namespace App\Exports;

use App\staff;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class StaffInfosExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return staff::query()->where('code_number', '=', $this->code_number);
    }

    // Check thử
    public function map($row): array
    {
        return [
            $row->id,
//            $row->index + 1,
            $row->schools->name,
            $row->units->name,
            $row->team_id === null ? 'Hiện chưa tham gia' : $row->teams->name,
            $row->types->name,
            $row->ranks->name,
            $row->positions->name,
            $row->full_name,
            $row->birthday,
            $row->code_number,
            $row->sex === 1 ? 'Nam' : 'Nữ' // 1: nam ; 0: nữ
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Đơn vị trên cơ sở',
            'Dơn vị cơ sở',
            'Đội/Đồn/Tổ',
            'Loại',
            'Cấp bậc',
            'Chức vụ',
            'Họ tên cán bộ',
            'Ngày sinh',
            'Số hiệu',
            'Giới tính',
        ];
    }
    //END Check thử

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Thông tin cán bộ';
    }
}

<?php

namespace App\Exports;

use App\discipline;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class DisciplinesExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number  = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return discipline::query()->where('code_number', '=', $this->code_number);

    }

    public function map($row): array
    {
        return [
            $row->id,
            $row->forms_of_discipline_id === '' ? 'Không' : $row->forms_of_disciplines->name,
            $row->decision_id,
            $row->decision_agencies->name
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Hình thức',
            'Số quyết định',
            'Cơ quan quyết định'
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Kỷ luật';
    }
}

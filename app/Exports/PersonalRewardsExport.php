<?php

namespace App\Exports;

use App\personal_reward;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PersonalRewardsExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    private $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number  = $code_number;
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return personal_reward::query()->where('code_number', '=', $this->code_number);

    }

    // Check thử
    public function map($row): array
    {
        return [
            $row->id,
//            $row->personal_title_id === '' ? 'Không' : $row->personal_titles->name,
            $row->forms_of_reward_id === '' ? 'Không' : $row->forms_of_rewards->name,
            $row->decision_id,
            $row->decision_agencies->name
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Danh hiệu cá nhân',
            'Hình thức',
            'Số quyết định',
            'Cơ quan quyết định'
        ];
    }
    //END Check thử

    /**
     * @return string
     */
    public function title(): string
    {
//        return 'Month ' . 'haha';
        return 'Khen thưởng cá nhân';
    }
}

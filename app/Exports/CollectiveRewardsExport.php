<?php

namespace App\Exports;

use App\collective_reward;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class CollectiveRewardsExport implements FromQuery, WithTitle, WithHeadings, WithMapping
{
    public function __construct()
    {
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return collective_reward::query()->where('unit_id', '=', auth()->user()->unit_id);
    }

    public function map($row): array
    {
        return [
            $row->id,
//            $row->collective_title_id === '' ? 'Không' : $row->collective_titles->name,
            $row->forms_of_reward_id === '' ? 'Không' : $row->forms_of_rewards->name,
            $row->decision_id,
            $row->decision_agencies->name
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'Danh hiệu tập thể',
            'Hình thức',
            'Số quyết định',
            'Cơ quan quyết định'
        ];
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return 'Khen thưởng tập thể';
    }
}

<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class InvoicesExport implements WithMultipleSheets
{
    use Exportable;

    protected $code_number;

    public function __construct(string $code_number)
    {
        $this->code_number = $code_number;
    }

    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $sheets[] = new StaffInfosExport($this->code_number);
        $sheets[] = new PersonalRewardsExport($this->code_number);
        $sheets[] = new CollectiveRewardsExport();
        $sheets[] = new DisciplinesExport($this->code_number);

        return $sheets;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class collective_reward extends Model
{
    protected $table = 'collective_rewards';

    protected $fillable = [
        'unit_id',
        'collective_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'unit_id',
        'collective_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function forms_of_rewards()
    {
        return $this->belongsTo(forms_of_reward::class, 'forms_of_reward_id','forms_of_reward_id');
    }

    public function collective_titles()
    {
        return $this->belongsTo(collective_title::class, 'collective_title_id','collective_title_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class discipline extends Model
{
    protected $table = 'disciplines';

    protected $fillable = [
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_of_discipline_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'code_number',
        'school_id',
        'unit_id',
        'team_id',
        'forms_of_discipline_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function forms_of_disciplines()
    {
        return $this->belongsTo(forms_of_discipline::class, 'forms_of_discipline_id','forms_of_discipline_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}

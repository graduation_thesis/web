<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personal_reward extends Model
{
    protected $table = 'personal_rewards';
    protected $fillable = [
        'staff_id',
        'personal_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'staff_id',
        'personal_title_id',
        'forms_of_reward_id',
        'decision_agency_id',
        'decision_id',
        'pending_remove',
    ];

    public function staffs()
    {
        return $this->belongsTo(staff::class, 'staff_id');
    }

    public function forms_of_rewards()
    {
        return $this->belongsTo(forms_of_reward::class, 'forms_of_reward_id','forms_of_reward_id');
    }

    public function personal_titles()
    {
        return $this->belongsTo(personal_title::class, 'personal_title_id','personal_title_id');
    }

    public function decision_agencies()
    {
        return $this->belongsTo(decision_agency::class, 'decision_agency_id','decision_agency_id');
    }

    public function decisions()
    {
        return $this->belongsTo(decision::class, 'decision_id','decision_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class staff extends Model
{
    protected $fillable = [
        'school_id',
        'unit_id',
        'team_id',
        'full_name',
        'birthday',
        'sex',
        'code_number',
        'type_staff_id',
        'rank_staff_id',
        'position_staff_id',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'school_id',
        'unit_id',
        'team_id',
        'full_name',
        'birthday',
        'sex',
        'code_number',
        'type_staff_id',
        'rank_staff_id',
        'position_staff_id',
        'pending_remove',
    ];

    public function personal_rewards()
    {
        return $this->hasMany(personal_reward::class);
    }

    public function schools()
    {
        return $this->belongsTo(school::class, 'school_id','school_id');
    }

    public function units()
    {
        return $this->belongsTo(unit::class, 'unit_id','unit_id');
    }

    public function teams()
    {
        return $this->belongsTo(team::class, 'team_id','id');
    }

    public function ranks()
    {
        return $this->belongsTo(rank_staff::class, 'rank_staff_id','rank_staff_id');
    }

    public function positions()
    {
        return $this->belongsTo(position_staff::class, 'position_staff_id','position_staff_id');
    }

    public function types()
    {
        return $this->belongsTo(type_staff::class, 'type_staff_id','type_staff_id');
    }
}
